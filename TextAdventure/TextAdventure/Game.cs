﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace TextAdventure
{
    public class Game
    {
        private String gameName = "N/A";   
        private bool shouldExit = false;
        private string correctAnswer;
        private int gameState;
        public Game()
        {
            
        }

        public void Setup()
        {
        
            gameName = gameName+ "Game";

            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Black;
        }

        public void Begin()
        {
            
            Console.WriteLine("---------------------------------");
            Console.WriteLine("---------------------------------");
            Console.WriteLine("-------Welcome to College--------");
            Console.WriteLine("---------------------------------");
            Console.WriteLine("---------------------------------");
            Console.WriteLine(" ");
            Console.WriteLine("You arrive at college. What would you like to do?");    
            Console.WriteLine("A: Go to the icebreakers.");
            Console.WriteLine("B: Go to Dorm");
            Console.WriteLine("C: Go to Town and get food.");
            Console.WriteLine("");
            Console.WriteLine("Type a,b or c.");  
            correctAnswer = "a";

            while (shouldExit == false)
            {
               
                String command = Console.ReadLine();    

                HandleCommand(command);

            }
        }

        public void HandleCommand(String command)
        {
           
            command = command.ToLower();
            if (command == correctAnswer)
            {
                Console.WriteLine("");
                Console.WriteLine("Congratulations, next question.");
                Console.WriteLine("");
                Console.WriteLine("");
                gameState++;
            }
            else
            {
                Console.WriteLine("Are you sure. Why not have another try?");
            }

            if (gameState == 0)
            {
                Console.WriteLine("You arrive at college. What would you like to do?");
                Console.WriteLine("A: Go to the icebreakers.");
                Console.WriteLine("B: Go to Dorm");
                Console.WriteLine("C: Go to Town and get food.");
                Console.WriteLine("");
                Console.WriteLine("Type a,b or c.");
            }
            else if (gameState == 1)
            {
                Console.WriteLine("You walk towards a group of people playing games.");
                Console.WriteLine("You have no idea what they are doing but you decide to join in. ");
                Console.WriteLine("What do you do next?");
                Console.WriteLine("a. You walk away.");
                Console.WriteLine("b. You join, and state your name, where you live and a unique interesting fact.");
                Console.WriteLine("c. You find a corner, sit down and don't talk for the next 2 hours.");

                correctAnswer = "b";
            }
            else if (gameState == 2)
            {
                Console.WriteLine("The icebreakers are fun, but you quickly get hungry.");
                Console.WriteLine("What is availible to eat?");
                Console.WriteLine("a. Fish and Chips ");
                Console.WriteLine("b. Burgers and Fries");
                Console.WriteLine("c. Cookies and Water");

                correctAnswer = "c";
            }
            else if (gameState == 3)
            {
                Console.WriteLine("You are soooo hungry but you dont want to be fat");
                Console.WriteLine("Do you take the food?");
                Console.WriteLine("c. Yes");
                Console.WriteLine("b. No");

                correctAnswer = "a";
            }
            else if (gameState == 4)
            {
                Console.WriteLine("The icebreakers end and the day has turned to night.");
                Console.WriteLine("You had no idea that 8 hours could go by so quickly.");
                Console.WriteLine("You leave and go to your room");
                Console.WriteLine("THANKS FOR PLAYING MY TEXT ADVENTURE!!!!");
            }
        }
    }
}
